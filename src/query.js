//Define queries.
import { gql } from "apollo-boost";

export const GET_TODOS = gql`
  {
    todos @client {
      id
      name
      completed
    }
  }
`;

export const ADD_TODO = gql`
  mutation addTodo($name: String!) {
    addTodo(name: $name) @client {
      name
    }
  }
`;